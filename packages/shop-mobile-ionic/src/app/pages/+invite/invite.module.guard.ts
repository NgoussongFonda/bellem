import { Injectable } from '@angular/core';
import { CanLoad, Route, Router } from '@angular/router';
import { Store } from '../../services/store.service';
import RegistrationSystem from '@modules/server.common/enums/RegistrationSystem';
import { UserRouter } from '@modules/client.common.angular2/routers/user-router.service';
import User from '@modules/server.common/entities/User';
import { takeUntil, first } from 'rxjs/operators';

@Injectable()
export class InviteModuleGuard implements CanLoad {
	public currentUser: User;
	private userId: string = '';

	constructor(
		private readonly store: Store,
		public userRouter: UserRouter,
		private readonly router: Router
	) {}

	canLoad(route: Route) {
		this.userId = this.store.userId;
		this.loadUser();
		return true;
		/*
		console.log('>>>>>>>>>>>>>FROM CONST... '+this.userId);


		if ( this.userId != null ){

			if ( this.currentUser != null || this.store.registrationSystem === RegistrationSystem.Disabled ) {
				this.router.navigate(['categories']);

				console.log('>>>>>>>>>>>>>UserId used : '+this.userId, this.currentUser);
				console.log('>>>>>>>>>>>>>UserId in Storage '+this.store.userId, this.currentUser);
				console.log('>>>>>>>>>>>>>We have a user '+this.currentUser, this.currentUser);
				return false;
			}
			else {
				console.log('>>>>>>>>>>>>>User is NULL ');
				//this.router.navigate(['invite/by-location']);
				return true;
			}
		}

		if ( this.store.registrationSystem === RegistrationSystem.Disabled ) {
			this.router.navigate(['categories']);
			return false;
		}

		//this.router.navigate(['invite/by-location']);
		return true;
		*/
	}

	private async loadUser() {
		const user = await this.userRouter
			.get(this.userId)
			.pipe(first())
			.toPromise();

		console.log('>>>>>>>>>>>>>User loaded : ' + user);

		this.currentUser = user as User;

		console.log('>>>>>>>>>>>>>FROM CONST... ' + this.userId);

		if (this.userId != null) {
			if (
				this.currentUser != null ||
				this.store.registrationSystem === RegistrationSystem.Disabled
			) {
				this.router.navigate(['categories']);

				console.log(
					'>>>>>>>>>>>>>UserId used : ' + this.userId,
					this.currentUser
				);
				console.log(
					'>>>>>>>>>>>>>UserId in Storage ' + this.store.userId,
					this.currentUser
				);
				console.log(
					'>>>>>>>>>>>>>We have a user ' + this.currentUser,
					this.currentUser
				);
				return false;
			} else {
				console.log('>>>>>>>>>>>>>User is NULL ');
				//this.router.navigate(['invite/by-location']);
				return true;
			}
		}

		if (this.store.registrationSystem === RegistrationSystem.Disabled) {
			this.router.navigate(['categories']);
			return false;
		}

		//this.router.navigate(['invite/by-location']);
		return true;
	}
}
